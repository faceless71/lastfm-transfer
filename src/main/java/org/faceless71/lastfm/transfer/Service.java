package org.faceless71.lastfm.transfer;

import de.umass.lastfm.*;
import de.umass.lastfm.scrobble.ScrobbleData;
import de.umass.lastfm.scrobble.ScrobbleResult;

import java.util.Collection;
import java.util.List;

class Service {
    // 80d1072aeec8e72f3e98a278bea239c5
    private static final String API_KEY = "ec01b5f12fe850baf9a3575181251204";
    // a47e9dfb5e4ab7b6fd830438322ce887
    private static final String API_SECRET = "ca55ebff1df55b2c7fa0360b2c86965a";

    //The number of results to fetch per page. Maximum is 200.
    private int tracksFetchLimitPerPage = 200;

    private Session session;

    Service() {
        Caller.getInstance().setUserAgent("faceless71/lastfm-transfer");
    }

    User getUserInfo(String username) {
        return User.getInfo(username, API_KEY);
    }

    int getUserTotalPages(String username) {
        return User.getRecentTracks(username, 1, tracksFetchLimitPerPage, API_KEY).getTotalPages();
    }

    Collection<Track> loadTracks(String username, int page) {
        return User.getRecentTracks(username, page, tracksFetchLimitPerPage, API_KEY).getPageResults();
    }

    Result authenticate(String username, String password) {
        session = Authenticator.getMobileSession(username, password, API_KEY, API_SECRET);
        return Caller.getInstance().getLastResult();
    }

    List<ScrobbleResult> scrobble(List<ScrobbleData> scrobbleDataList) {
        return Track.scrobble(scrobbleDataList, session);
    }
}
