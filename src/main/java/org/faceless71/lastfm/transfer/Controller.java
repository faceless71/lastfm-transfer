package org.faceless71.lastfm.transfer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import de.umass.lastfm.Caller;
import de.umass.lastfm.Track;
import de.umass.lastfm.User;
import de.umass.lastfm.scrobble.ScrobbleData;
import de.umass.lastfm.scrobble.ScrobbleResult;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Controller {
    public PasswordField passwordField;
    public TextField usernameField2;
    public Button loadButton;
    public Label loadProgressBarLabel;
    public ProgressBar loadProgressBar;
    public TextField usernameField1;
    public TextArea logTextArea;
    public ProgressBar uploadProgressBar;
    public Label uploadProgressBarLabel;
    public Button chooseFileButton;
    public Button helpButton;

    private Service service;
    private User user1;
    private User user2;

    public Controller() {
        service = new Service();
    }

    public void loginButton1Click(ActionEvent actionEvent) {
        user1 = service.getUserInfo(usernameField1.getText());
        if (Caller.getInstance().getLastResult().isSuccessful()) {
            loadButton.setDisable(false);
            addLogText("Connected to: " + user1.getName());
            addLogText("Total tracks: " + user1.getPlaycount());
        } else {
            addLogText("Error! " + Caller.getInstance().getLastResult().getErrorMessage());
        }
    }

    public void loadButtonClick(ActionEvent actionEvent) {
        loadProgressBar.setVisible(true);
        loadTracks(user1);
    }

    public void helpButtonClick(MouseEvent mouseEvent) {
        addLogText("NOTE: If Timestamp of the track is older than 2 weeks, " +
                "it will be set to the current time, due to Last.fm API limitations.");
    }

    public void loginButton2Click(ActionEvent actionEvent) {
        if (service.authenticate(usernameField2.getText(), passwordField.getText()).isSuccessful()) {
            user2 = service.getUserInfo(usernameField2.getText());
            chooseFileButton.setDisable(false);
            addLogText("Connected to: " + user2.getName());
        } else {
            addLogText("Error! " + Caller.getInstance().getLastResult().getErrorMessage());
        }
    }

    public void chooseFileButtonClick(ActionEvent actionEvent) {
        uploadProgressBar.setVisible(true);

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open file with tracks");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Text Files", "*.txt"),
                new FileChooser.ExtensionFilter("All Files", "*.*"));
        File selectedFile = fileChooser.showOpenDialog(chooseFileButton.getScene().getWindow());
        if (selectedFile != null) {
            uploadScrobbleDataList(readFileWithTracks(selectedFile));
        }
    }

    /**
     * Load tracks from Last.fm for given user and save it to file.
     *
     * @param user input user.
     */
    private void loadTracks(User user) {
        new Thread(() -> {
            Collection<Track> tracks = new ArrayList<>();
            int totalPages = service.getUserTotalPages(user.getName());
            for (int page = 1; page <= totalPages; page++) {
                tracks.addAll(service.loadTracks(user.getName(), page));
                loadProgressBar.setProgress((double) page / totalPages);
                int currentPage = page;
                Platform.runLater(() -> loadProgressBarLabel.setText(Math.round(100 * (double) currentPage / totalPages) + "%"));
            }

            createFileWithTracks(tracks);
        }).start();
    }

    /**
     * Creating file (username.txt) with tracks in JSON format.
     *
     * @param tracks input Collection of tracks.
     */
    private void createFileWithTracks(Collection<Track> tracks) {
        String tracksJson = new GsonBuilder()
                .setPrettyPrinting()
                .disableHtmlEscaping()
                .create()
                .toJson(tracksToScrobbleDataList(tracks));
        File file = new File(user1.getName() + ".txt");
        try {
            Files.write(Paths.get(file.toURI()), tracksJson.getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE);
        } catch (IOException e) {
            addLogText("Error! " + e.getMessage());
        }
        addLogText(MessageFormat.format("Tracks loaded {0}. Saved to file {1}", tracks.size(), file.getAbsolutePath()));
    }

    /**
     * Reading file with tracks in JSON format.
     *
     * @param file input file.
     * @return list of ScrobbleData.
     */
    private List<ScrobbleData> readFileWithTracks(File file) {
        addLogText("File with tracks selected: " + file.getAbsolutePath());
        try {
            JsonReader reader = new JsonReader(new FileReader(file));
            return new Gson().fromJson(reader, new TypeToken<List<ScrobbleData>>() {}.getType());
        } catch (IOException e) {
            addLogText("Error! " + e.getMessage());
        }
        return null;
    }

    /**
     * Uploading(scrobbling) data in batches of 50.
     *
     * @param scrobbleDataList input list of ScrobbleData.
     */
    private void uploadScrobbleDataList(List<ScrobbleData> scrobbleDataList) {
        addLogText("Uploading tracks.");
        new Thread(() -> {
            for (int i = 0; i < Math.ceil(scrobbleDataList.size() / 50); i++) {
                double progress = (double) (i + 1) / Math.ceil(scrobbleDataList.size() / 50);
                uploadProgressBar.setProgress(progress);
                Platform.runLater(() -> uploadProgressBarLabel.setText(Math.round(progress * 100) + "%"));

                List<ScrobbleResult> results = service.scrobble(scrobbleDataList.subList(50 * i, 50 * (i + 1)));
                results.forEach(result -> {
                    if (result.isIgnored()) {
                        addLogText("Error! Ignored " + result.getIgnoredMessage());
                    } else if (!result.isSuccessful()) {
                        addLogText("Error! " + result.getErrorMessage());
                    }
                });
            }
            addLogText(MessageFormat.format("Uploaded {0} tracks to the {1} account.", scrobbleDataList.size(), user2.getName()));
        }).start();
    }

    /**
     * Converting Track objects to ScrobbleData objects. If Timestamp of the track is older than 2 weeks, setting
     * it to the current time, due to Last.fm API limitations.
     *
     * @param tracks input Collection of tracks.
     * @return list of ScrobbleData.
     */
    private List<ScrobbleData> tracksToScrobbleDataList(Collection<Track> tracks) {
        return tracks.stream()
                .map(track -> {
                    ScrobbleData data = new ScrobbleData();
                    data.setArtist(track.getArtist());
                    data.setTrack(track.getName());
                    if (track.getPlayedWhen().before(new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(14))))
                        data.setTimestamp((int) TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));
                    else
                        data.setTimestamp((int) TimeUnit.MILLISECONDS.toSeconds(track.getPlayedWhen().getTime()));
                    return data;
                })
                .collect(Collectors.toList());
    }

    /**
     * Simple method to add log text to the logTextArea.
     *
     * @param text input text.
     */
    private void addLogText(String text) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        logTextArea.appendText(dtf.format(now) + " " + text + "\n");
    }
}
